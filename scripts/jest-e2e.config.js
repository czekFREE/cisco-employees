// Configarion for running jest for end-to-end tests

console.log('jest.e2e.test.js');


module.exports = {
    preset: 'jest-puppeteer-preset',
    collectCoverageFrom: ['src/**/*.{js,jsx,mjs}'],
    setupFiles: ['<rootDir>/config/polyfills.js'],
    testMatch: [
        '<rootDir>/src/**/?(*.)(e2e-test).{js,jsx,mjs}',
    ],
    testPathIgnorePatterns: [
        "<rootDir>/src/**/__tests__/**/*.{js,jsx,mjs}",
        "<rootDir>/src/**/?(*.)(spec|test).{js,jsx,mjs}"
    ],
    testEnvironment: 'node',
    testURL: 'http://localhost',
    transform: {
        '^.+\\.(js|jsx|mjs)$': '<rootDir>/node_modules/babel-jest',
        '^.+\\.css$': '<rootDir>/config/jest/cssTransform.js',
        '^(?!.*\\.(js|jsx|mjs|css|json)$)': '<rootDir>/config/jest/fileTransform.js',
    },
    transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs)$'],
    moduleNameMapper: {
        '^react-native$': 'react-native-web',
    },
    moduleFileExtensions: ['web.js', 'js', 'json', 'web.jsx', 'jsx', 'node', 'mjs'],
    setupTestFrameworkScriptFile: '<rootDir>/src/setupTests.js',
};
