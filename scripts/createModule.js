const path          = require('path');
const fsExtra       = require('fs-extra');
const _             = require('lodash');

const rootDir       = path.resolve('.', 'src', 'modules');
const componentName = _.upperFirst(process.argv[2]);
const componentDir  = path.join(rootDir, componentName);

const emptyFileContents = `// @flow
`;

/**
 * Module constants
 */
const constantsFilePath = path.join(componentDir, 'constants.js');
const constantsFileContents = `// @flow
export const NAME = '${componentName}';
`;

/**
 * Module ActionTypes
 */
const actionTypesFilePath = path.join(componentDir, 'actionTypes.js');
const actionTypesFileContents =  `// @flow
import { NAME } from './constants';

const SAMPLE_ACTION = \`\$\{NAME}/SAMPLE_ACTION\`;
`;

/**
 * Module ActionCreators
 */
const actionsFilePath = path.join(componentDir, 'actions.js');
const actionsFileContents = emptyFileContents;

/**
 * Module Mocks
 */
const mockFilePath = path.join(componentDir, 'mock.js');
const mockFileContents = emptyFileContents;

/**
 * Module Reducer
 */
const reducerFilePath = path.join(componentDir, 'reducer.js');
const reducerFileContents = `// @flow
import actionTypes from './actionTypes';


const initialState = {};


export default (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
};
`;

/**
 * Module Selectors
 */
const selectorsFilePath = path.join(componentDir, 'selectors.js');
const selectorsFileContents = `// @flow
import { NAME } from './constants';
`;

/**
 * Module Model
 */
const modelFilePath = path.join(componentDir, 'model.js');
const modelFileContents = emptyFileContents;

/**
 * Module Component
 */
const primaryComponentFilePath = path.join(componentDir, 'components', `${componentName}.js`);
const primaryComponentFileContents = `// @flow
import React, { PureComponent } from 'react';
import * as classes from './${componentName}.scss';


type Props = {};


export default class ${componentName} extends PureComponent<Props> {
    render() {
        return (
            <div className={classes.${componentName}}>Hello world</div>
        );
    }
}
`;

/**
 * Module Component styles
 */
const primaryComponentStylesFilePath = path.join(componentDir, 'components', `${componentName}.scss`);
const primaryComponentStylesFileContents = `.${componentName} {

}
`;


/**
 * Component container
 */
const primaryComponentContainerFilePath = path.join(componentDir, 'components', `${componentName}Container.js`);
const primaryComponentContainerFileContents = `// @flow
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ${componentName} from './${componentName}';
import * as actions from '../actions';


function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(${componentName});
`;


/**
 * Components index
 */
const componentsIndexFilePath = path.join(componentDir, 'components', 'index.js');
const componentsIndexContents = `// @flow
import ${componentName} from './${componentName}';

export default {
    ${componentName},
};
`;


/**
 * Index
 */
const indexFilePath = path.join(componentDir, 'index.js');
const indexFileContents = `// @flow
import components from './components';
import reducer from './reducer';
import * as actions from './actions';
import * as constants from './constants';
import * as selectors from './selectors';


export default { actions, reducer, components, constants };
`;


async function createComponent() {
    const componentDirExists = await fsExtra.pathExists(componentDir);

    if (componentDirExists) {
        throw new Error(`componentDir ${componentDir} already exists - aborting !!!`);
    }

    return Promise.all([
        fsExtra.outputFile(constantsFilePath, constantsFileContents),
        fsExtra.outputFile(actionTypesFilePath, actionTypesFileContents),
        fsExtra.outputFile(actionsFilePath, actionsFileContents),
        fsExtra.outputFile(mockFilePath, mockFileContents),
        fsExtra.outputFile(modelFilePath, modelFileContents),
        fsExtra.outputFile(indexFilePath, indexFileContents),
        fsExtra.outputFile(reducerFilePath, reducerFileContents),
        fsExtra.outputFile(selectorsFilePath, selectorsFileContents),

        fsExtra.outputFile(primaryComponentFilePath, primaryComponentFileContents),
        fsExtra.outputFile(primaryComponentContainerFilePath, primaryComponentContainerFileContents),
        fsExtra.outputFile(primaryComponentStylesFilePath, primaryComponentStylesFileContents),
        fsExtra.outputFile(componentsIndexFilePath, componentsIndexContents),
    ]);
}


createComponent().catch(error => console.error(error.message));
