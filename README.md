# Cisco job interview sample application

## Base dependencies
  - Git
  - Node.js v8.9+
  - Yarn 1.7.0 (project is based on create-react-app & I had troubles installing dependencies with npm)

## Install instructions
    1. git clone https://czekFREE@bitbucket.org/czekFREE/cisco-employees.git
    2. cd cisco-employees
    3. yarn install
    4. yarn start

## Test instructions
    1. yarn test
    2. yarn test:coverage

## Build info & project structure
    - This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
    - Project  structure is inspired by [Jack Hsu](https://jaysoo.ca/2016/02/28/organizing-redux-application/).
    - Actions tend to be [FSA-compliant](https://github.com/redux-utilities/flux-standard-action).

## Important flaws of this project
    - Project is not tested as it should be
    - Project was developed only on Chrome running on Ubuntu 18.04 (cannot guarantee cross-browser compatibility)
    - Project is not optimized for responsive views (cannot guarantee mobile-friendlines & onResize responsivity)
    - Project does not contain proper typings, most importantly props types
