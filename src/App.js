import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { loadTheme } from 'office-ui-fabric-react';
import { initializeIcons } from '@uifabric/icons';
import store, { history } from './store';
import Routes from './routes';
import appBarModule from './components/AppBar';
import appTheme from './App.theme.js';
import './normalize.css';
import classes from './App.css';

// import * as officeUi from 'office-ui-fabric-react';
// console.log('officeUi', officeUi);
const { AppBar } = appBarModule.components;


loadTheme({ palette: appTheme });
initializeIcons();


class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <div className={classes.App}>
                        <AppBar />

                        <main>
                            <Routes/>
                        </main>
                    </div>
                </ConnectedRouter>
            </Provider>
        );
    }
}

export default App;
