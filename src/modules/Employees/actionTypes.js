// @flow
import { NAME } from './constants';

export const FETCH_DATA_REQUEST = `${NAME}/FETCH_DATA_REQUEST`;
export const SORT_PRIMARY_TABLE = `${NAME}/SORT_PRIMARY_TABLE`;
export const FETCH_DATA_SUCCESS = `${NAME}/FETCH_DATA_SUCCESS`;
export const ADD_DATA_ITEM      = `${NAME}/ADD_DATA_ITEM`;
