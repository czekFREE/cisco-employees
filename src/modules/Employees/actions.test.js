import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { SORT_PRIMARY_TABLE, FETCH_DATA_REQUEST, FETCH_DATA_SUCCESS, ADD_DATA_ITEM }   from './actionTypes';
import { sortedNaturaly, newDataItem } from './mock';
import { onSortPrimaryTable, onFetchDataRequest, onFetchDataSuccess, onAddDataItem } from './actions';
import { NAME, ASC } from './constants';
import { initialState } from './reducer';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


describe('Employees actions', () => {
    it(`onSortPrimaryTable should fire proper ${SORT_PRIMARY_TABLE} action`, () => {
        const expectedAction = { type: SORT_PRIMARY_TABLE, payload: { sortProp: ASC } };
        const actualAction = onSortPrimaryTable(ASC);

        expect(actualAction).toEqual(expectedAction);
    });

    it(`onFetchDataRequest should fire proper ${FETCH_DATA_REQUEST} action`, () => {
        const expectedActions = [
            { type: FETCH_DATA_REQUEST, payload: {} },
            { type: FETCH_DATA_SUCCESS, payload: { data: sortedNaturaly } },
        ];

        const store = mockStore({ [NAME]: initialState });

        return store.dispatch(onFetchDataRequest()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });


    it(`onFetchDataSuccess should fire proper ${FETCH_DATA_SUCCESS} action`, () => {
        const expectedAction = { type: FETCH_DATA_SUCCESS, payload: { data: sortedNaturaly } };
        const actualAction = onFetchDataSuccess(sortedNaturaly);

        expect(actualAction).toEqual(expectedAction);
    });

    it(`onAddDataItem should fire proper ${ADD_DATA_ITEM} action`, () => {
        const expectedAction = { type: ADD_DATA_ITEM, payload: { data: newDataItem } };
        const actualAction = onAddDataItem(newDataItem);

        expect(actualAction).toEqual(expectedAction);
    });
});


