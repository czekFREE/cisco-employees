// @flow
import _ from 'lodash';
import { SORT_PRIMARY_TABLE, FETCH_DATA_SUCCESS, ADD_DATA_ITEM }   from './actionTypes';
import { ASC, DESC } from './constants';


export const initialState = {
    isLoaded: false,
    isReady: false,
    data: [],
    sortData: [],
    sortProp: '',
    sortType: '',
};


export default (state = initialState, action) => {
    switch (action.type) {
        case SORT_PRIMARY_TABLE:
            const { sortProp } = action.payload;

            if (!state.sortType) {
                return Object.assign({}, state, {
                    sortProp,
                    sortType: ASC,
                    sortData: _.sortBy(state.data, item => item[sortProp] && item[sortProp].toLowerCase()),
                });
            } else if (state.sortType === ASC) {
                return Object.assign({}, state, {
                    sortProp,
                    sortType: DESC,
                    sortData: _.reverse(state.sortData),
                });
            } else {
                return Object.assign({}, state, {
                    sortProp: '',
                    sortType: '',
                    sortData: [ ...state.data ],
                });
            }

        case FETCH_DATA_SUCCESS:
            return Object.assign({}, state, {
                isLoaded: true,
                isReady: true,
                data: [ ...state.data, ...action.payload.data ],
                sortData: [ ...state.data, ...action.payload.data ],
            });

        case ADD_DATA_ITEM:
            return Object.assign({}, state, {
                data: [ ...state.data, action.payload.data ],
                sortData: [ ...state.data, action.payload.data ],
                sortProp: '',
                sortType: '',
            });


        default:
            return state;
    }
};
