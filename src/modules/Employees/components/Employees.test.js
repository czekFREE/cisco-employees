import React from 'react';
import Employees from './Employees';
import { shallow } from 'enzyme';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react';


describe('<Employees />', () => {
    it('renders with loading spiner', () => {
        const wrapper = shallow(<Employees onFetchDataRequest={() => {}} />);
        expect(wrapper.find('.Employees').length).toBe(1);
        expect(wrapper.contains(<Spinner label="Loading employees..." size={SpinnerSize.large} />)).toBe(true);
    });

    it('renders dashboard wrapper when data are ready', () => {
        const wrapper = shallow(<Employees onFetchDataRequest={() => {}} isReady />);
        expect(wrapper.find('.Employees__dashboard').length).toBe(1);
        expect(wrapper.find('.Employees__pieChart').length).toBe(1);
        expect(wrapper.find('.Employees__barChart').length).toBe(1);
    });
});
