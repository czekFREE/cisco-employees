// @flow
import React, { Component } from 'react';
import * as classes from './Employees.css';
import { DefaultButton } from 'office-ui-fabric-react';
import _ from 'lodash';
import { DetailsList, DetailsListLayoutMode, SelectionMode } from 'office-ui-fabric-react';
import { Sticky } from 'office-ui-fabric-react';
import { ScrollablePane } from 'office-ui-fabric-react';
import { ResponsiveContainer, PieChart, Pie, BarChart, Bar, XAxis, YAxis, Tooltip } from 'recharts';
import { DESC } from '../constants';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react';
import { NavLink } from 'react-router-dom';


const NAME = 'name';
const JOB_TITLE = 'jobTitle';
const TENURE = 'tenure';
const GENDER = 'gender';


type Props = {
    isReady: boolean,
    data: array,
    sortData: array,
    sortProp: string,
    sortType: string,
    onFetchDataRequest: () => void,
};


export default class Employees extends Component<Props> {
    createEmployeeButtonIconProps = { iconName: 'Add' };
    pieChartMargin = { top: 15, bottom: 15, };
    detailsList = React.createRef();

    componentDidMount() {
        this.props.onFetchDataRequest();
    }

    onColumnHeaderClick = (event = {}, column = {}) => {
        const { onSortPrimaryTable } = this.props;
        const { fieldName } = column;

        if (fieldName && onSortPrimaryTable) {
            onSortPrimaryTable(fieldName);
        }
    }

    onRenderDetailsHeader = (detailsHeaderProps, defaultRender) => (
        <Sticky>{defaultRender(detailsHeaderProps)}</Sticky>
    )

    getPieChartLabel = data => data.jobTitle;

    getColumns = () => {
        const { sortProp, sortType } = this.props;

        return [
            {
                key: NAME,
                headerClassName: classes.dashboardTableHeaderItem,
                name: 'Name',
                fieldName: NAME,
                isResizable: true,
                isSorted: sortProp === NAME,
                isSortedDescending: sortProp === NAME && sortType === DESC,
            },
            {
                key: JOB_TITLE,
                headerClassName: classes.dashboardTableHeaderItem,
                name: 'Job title',
                fieldName: JOB_TITLE,
                isResizable: true,
                isSorted: sortProp === JOB_TITLE,
                isSortedDescending: sortProp === JOB_TITLE && sortType === DESC,
            },
            {
                key: TENURE,
                headerClassName: classes.dashboardTableHeaderItem,
                name: 'Tenure',
                fieldName: TENURE,
                isResizable: true,
                isSorted: sortProp === TENURE,
                isSortedDescending: sortProp === TENURE && sortType === DESC,
            },
            {
                key: GENDER,
                headerClassName: classes.dashboardTableHeaderItem,
                name: 'Gender',
                fieldName: GENDER,
                isResizable: true,
                isSorted: sortProp === GENDER,
                isSortedDescending: sortProp === GENDER && sortType === DESC,
            },
        ];
    }

    render() {
        const { data, sortData, isReady } = this.props;

        if (!isReady) {
            return (
                <div className={`Employees ${classes.SpinnerWrapper}`}>
                    <Spinner label="Loading employees..." size={SpinnerSize.large} />
                </div>
            );
        }

        const employeesByGenderData = _.chain(data)
            .countBy('gender')
            .map((value, key) => ({ gender: key, count: value }))
            .value();

        const employeesByJobTitleData = _.chain(data)
            .countBy('jobTitle')
            .map((value, key) => ({ jobTitle: key, count: value }))
            .value();

        return (
            <div className={`Employees ${classes.Employees} ms-scaleUpIn100`}>
                <div className={`Employees__dashboard ${classes.dashboardCard} ${classes.dashboardCardTable}`}>
                    <div className={`${classes.dashboardCardHeader} ${classes.dashboardCardTableHeader}`}>
                        <span>Corporate employees</span>
                        <NavLink to="/employees/create">
                            <DefaultButton
                                id="create-employee-button"
                                data-automation-id="Create-employee"
                                text="Create-employee"
                                iconProps={this.createEmployeeButtonIconProps}
                                primary
                            />
                        </NavLink>
                    </div>
                    <div className={`${classes.dashboardCardBody} ${classes.dashboardTableBody}`}>
                        <ScrollablePane className={classes.dashboardTableBodyScrollContainer}>
                            <DetailsList
                                items={sortData}
                                columns={this.getColumns()}
                                layoutMode={DetailsListLayoutMode.justified}
                                selectionMode={SelectionMode.none}
                                isHeaderVisible
                                selectionPreservedOnEmptyClick
                                enterModalSelectionOnTouch
                                ref={this.detailsList}
                                onRenderDetailsHeader={this.onRenderDetailsHeader}
                                onColumnHeaderClick={this.onColumnHeaderClick}
                            />
                        </ScrollablePane>
                    </div>
                </div>

                <div className={`Employees__pieChart ${classes.dashboardCard} ${classes.dashboardGraph1}`}>
                    <div className={classes.dashboardCardHeader}>
                        <span>Employees by job title</span>
                    </div>

                    <div className={`${classes.dashboardCardBody} ${classes.dashboardGraph1Body}`}>
                        <ResponsiveContainer height="100%">
                            <PieChart margin={this.pieChartMargin}>
                                <Pie
                                    data={employeesByJobTitleData}
                                    dataKey="count"
                                    nameKey="jobTitle"
                                    cx="50%"
                                    cy="50%"
                                    fill="#C6C7CA"
                                />
                                <Pie
                                    data={employeesByJobTitleData}
                                    dataKey="count"
                                    nameKey="jobTitle"
                                    cx="50%"
                                    cy="50%"
                                    innerRadius={70}
                                    fill="#049FD9"
                                    label={this.getPieChartLabel}
                                />
                                <Tooltip />
                            </PieChart>
                        </ResponsiveContainer>
                    </div>
                </div>

                <div className={`Employees__barChart ${classes.dashboardCard} ${classes.dashboardGraph2}`}>
                    <div className={classes.dashboardCardHeader}>
                        <span>Employees by gender</span>
                    </div>
                    <div className={`${classes.dashboardCardBody} ${classes.dashboardGraph2Body}`}>
                        <ResponsiveContainer width="100%">
                            <BarChart data={employeesByGenderData}>
                                <XAxis dataKey="gender" />
                                <YAxis dataKey="count" />
                                <Tooltip cursor={false}/>
                                <Bar dataKey="count" name="Gender" fill="#049FD9"/>
                            </BarChart>
                        </ResponsiveContainer>
                    </div>
                </div>
            </div>
        );
    }
}
