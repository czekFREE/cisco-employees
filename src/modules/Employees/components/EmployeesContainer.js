// @flow
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Employees from './Employees';
import * as actions from '../actions';
import { getSortProp, getSortType, getData, getSortData, getIsReady } from '../selectors';

function mapStateToProps(state) {
    return {
        isReady: getIsReady(state),
        data: getData(state),
        sortData: getSortData(state),
        sortProp: getSortProp(state),
        sortType: getSortType(state),
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Employees);
