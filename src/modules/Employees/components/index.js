// @flow
import EmployeesContainer from './EmployeesContainer';

export default {
    Employees: EmployeesContainer,
};
