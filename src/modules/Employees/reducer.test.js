import reducer, { initialState as defaultState } from './reducer';
import { SORT_PRIMARY_TABLE, FETCH_DATA_SUCCESS, ADD_DATA_ITEM }   from './actionTypes';
import { sortedNaturaly, sortedByNameAsc, sortedByNameDesc, newDataItem } from './mock';
import { onSortPrimaryTable, onFetchDataSuccess, onAddDataItem } from './actions';
import { ASC, DESC } from './constants';


describe('Employees reducer', () => {
    it('should return the initial state', () => {
        const expectedStateValue = { ...defaultState };
        const actualStateValue = reducer(undefined, {});

        expect(actualStateValue).toEqual(expectedStateValue);
    });

    describe(`when ${SORT_PRIMARY_TABLE} action is fired`, () => {
        it('should sort primary table by ASC when sortType is not defined', () => {
            const initialState = { ...defaultState, data: sortedNaturaly };
            const actualStateValue = reducer(initialState, onSortPrimaryTable('name'));

            expect(actualStateValue.sortType).toEqual(ASC);
            expect(actualStateValue.sortData).toEqual(sortedByNameAsc);
        });

        it('should sort primary table by DESC when sortType is ASC', () => {
            const initialState = { ...defaultState, data: sortedNaturaly, sortProp: 'name', sortType: ASC, sortData: sortedByNameDesc };
            const actualStateValue = reducer(initialState, onSortPrimaryTable('name'));

            expect(actualStateValue.sortType).toEqual(DESC);
            expect(actualStateValue.sortData).toEqual(sortedByNameDesc);
        });

        it('should sort primary table by natural order when SORT_PRIMARY_TABLE action is fired and sortType is DESC', () => {
            const initialState = { ...defaultState, data: sortedNaturaly, sortProp: 'name', sortType: DESC, sortData: sortedByNameDesc };
            const actualStateValue = reducer(initialState, onSortPrimaryTable('name'));

            expect(actualStateValue.sortType).toEqual('');
            expect(actualStateValue.sortData).toEqual(sortedNaturaly);
        });
    });

    describe(`when ${FETCH_DATA_SUCCESS} action is fired`, () => {
        it('should mark state as loaded', () => {
            const initialState = { ...defaultState };
            const actualStateValue = reducer(initialState, onFetchDataSuccess(sortedNaturaly));

            expect(actualStateValue.isLoaded).toEqual(true);
        });

        it('should mark state as ready', () => {
            const initialState = { ...defaultState };
            const actualStateValue = reducer(initialState, onFetchDataSuccess(sortedNaturaly));

            expect(actualStateValue.isReady).toEqual(true);
        });

        it('should save data to state', () => {
            const initialState = { ...defaultState };

            const expectedStateValue = { ...defaultState, data: [ ...sortedNaturaly ] };
            const actualStateValue = reducer(initialState, onFetchDataSuccess(sortedNaturaly));

            expect(actualStateValue.data).toEqual(expectedStateValue.data);
        });
    });

    describe(`when ${ADD_DATA_ITEM} action is fired`, () => {
        it('should concatenate prev data to newly fetched & reset sort data', () => {
            const initialState = { ...defaultState, data: [ ...sortedNaturaly ], sortData: [ ...sortedByNameAsc ] };

            const expectedStateValue = { ...defaultState, data: [ ...sortedNaturaly, newDataItem ], sortData: [ ...sortedNaturaly, newDataItem ] };
            const actualStateValue = reducer(initialState, onAddDataItem(newDataItem));

            expect(actualStateValue.data).toEqual(expectedStateValue.data);
        });
    });
});


