// @flow
import { SORT_PRIMARY_TABLE, FETCH_DATA_REQUEST, FETCH_DATA_SUCCESS, ADD_DATA_ITEM } from './actionTypes';
import { getIsLoadedStatus } from './selectors';
import mockData from './mock';

export const onSortPrimaryTable = sortProp => ({ type: SORT_PRIMARY_TABLE, payload: { sortProp } });
export const onFetchDataSuccess = data => ({ type: FETCH_DATA_SUCCESS, payload: { data } });

// Async fetch mock
export const onFetchDataRequest = () => (dispatch, getState) => {
    const dataAreLoaded = getIsLoadedStatus(getState());
    dispatch({ type: FETCH_DATA_REQUEST, payload: {} });

    if (dataAreLoaded) { return; }
    const randomTimeout = Math.round(Math.random() * 1500);

    return Promise.resolve(resolve => setTimeout(resolve, randomTimeout)).then(() => {
        return dispatch(onFetchDataSuccess(mockData));
    });
};

export const onAddDataItem = data => ({ type: ADD_DATA_ITEM, payload: { data } });
