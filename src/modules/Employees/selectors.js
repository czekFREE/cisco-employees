// @flow
import { NAME } from './constants';


export const getSortProp        = state => state[NAME].sortProp;
export const getSortType        = state => state[NAME].sortType;
export const getData            = state => state[NAME].data;
export const getSortData        = state => state[NAME].sortData;
export const getIsReady         = state => state[NAME].isReady;
export const getIsLoadedStatus  = state => state[NAME].isLoaded;


