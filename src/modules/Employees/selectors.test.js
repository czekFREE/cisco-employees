import { initialState } from './reducer';
import { NAME } from './constants';
import {
    getSortProp,
    getSortType,
    getData,
    getSortData,
    getIsReady,
    getIsLoadedStatus,
} from './selectors';

const stateMock = {
    [NAME]: initialState,
};

describe('Employees selectors', () => {
    it('getSortProp() should return sortProp', () => {
        expect(getSortProp(stateMock)).toEqual('');
    });

    it('getSortType() should return sortType', () => {
        expect(getSortType(stateMock)).toEqual('');
    });

    it('getData() should return data', () => {
        expect(getData(stateMock)).toEqual([]);
    });

    it('getSortData() should return sortData', () => {
        expect(getSortData(stateMock)).toEqual([]);
    });

    it('getIsReady() should return isReady', () => {
        expect(getIsReady(stateMock)).toEqual(false);
    });

    it('getIsLoadedStatus() should return isLoaded', () => {
        expect(getIsLoadedStatus(stateMock)).toEqual(false);
    });
});


