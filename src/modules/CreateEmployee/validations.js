export const required = value => (value ? undefined : 'Required');

export const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined;

export const minLength = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined;

export const number = value => (value && isNaN(Number(value)) ? 'Must be a number' : undefined);

export const gender = value => (value && (value.toLowerCase() === 'male' || value.toLowerCase() === 'female') ? 'Must be either Male or Female' : undefined);
