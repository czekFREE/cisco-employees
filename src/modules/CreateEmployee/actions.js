// @flow
import { SUBMIT_FORM_REQUEST } from './actionTypes';

import employeesModule from '../Employees';


const { onAddDataItem } = employeesModule.actions;

// Async save mock
export const onSubmitFormRequest = newEmployee => (dispatch, getState) => {
    dispatch({ type: SUBMIT_FORM_REQUEST, payload: { data: newEmployee } });
    const randomTimeout = Math.round(Math.random() * 1500);

    return Promise.resolve(resolve => setTimeout(resolve, randomTimeout)).then(() => {
        return dispatch(onSubmitFormSuccess(newEmployee));
    });
};

export const onSubmitFormSuccess = newEmployee => onAddDataItem(newEmployee);
