import reducer, { initialState as defaultState } from './reducer';

describe('CreateEmployee reducer', () => {
    it('should return the initial state', () => {
        const expectedStateValue = { ...defaultState };
        const actualStateValue = reducer(undefined, {});

        expect(actualStateValue).toEqual(expectedStateValue);
    });
});


