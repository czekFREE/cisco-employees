import React from 'react';
import CreateEmployee from './CreateEmployee';
import { shallow } from 'enzyme';


describe('<CreateEmployee />', () => {
    it('renders', () => {
        const wrapper = shallow(<CreateEmployee handleSubmit={() => {}}/>);
        expect(wrapper.find('.CreateEmployee').length).toBe(1);
    });
});
