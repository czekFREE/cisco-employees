// @flow
import CreateEmployeeContainer from './CreateEmployeeContainer';

export default {
    CreateEmployee: CreateEmployeeContainer,
};
