// @flow
import React, { Component } from 'react';
import { DefaultButton } from 'office-ui-fabric-react';
import { Field } from 'redux-form';
import { NavLink } from 'react-router-dom';
import { Icon } from 'office-ui-fabric-react';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react';
import * as classes from './CreateEmployee.css';
import { required, maxLength, minLength, number } from '../validations';
import reduxFormTextFieldRenderer from '../../../components/ReduxFormTextFieldRenderer';
import reduxFormDropdownRenderer from '../../../components/ReduxFormDropdownRenderer';

const { ReduxFormTextFieldRenderer } = reduxFormTextFieldRenderer.components;
const { ReduxFormDropdownRenderer } = reduxFormDropdownRenderer.components;


type Props = {
    onSubmitFormRequest: () => void,
    handleSubmit: () => void,
};


const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
const maxLength15 = maxLength(15);
const minLength4 = minLength(4);


export default class CreateEmployee extends Component<Props> {
    constructor(props) {
        super(props);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    genderOptions = [{ key: 'Male', text: 'Male' }, { key: 'Female', text: 'Female' }];
    backButtonIconProps = { iconName: 'Back' };
    stornoButtonIconProps = { iconName: 'Cancel' };
    saveButtonIconProps = { iconName: 'Accept' };

    onFormSubmit = async function(values) {
        await sleep(Math.floor(Math.random() * 2000));

        this.props.onSubmitFormRequest(values);
    };

    render() {
        const { handleSubmit, reset, pristine, submitting, invalid, submitSucceeded } = this.props;

        return (
            <form className={`CreateEmployee ${classes.CreateEmployee} ms-scaleUpIn100`} onSubmit={handleSubmit(this.onFormSubmit)}>
                <h2 className={classes.formHeading}>New employee form</h2>
                <div className={classes.formBody}>
                    <Field name="name" component={ReduxFormTextFieldRenderer} label="Name" validate={[required, minLength4, maxLength15]} />
                    <Field name="jobTitle" component={ReduxFormTextFieldRenderer} label="Job title" validate={[required, minLength4, maxLength15]}/>
                    <Field name="tenure" component={ReduxFormTextFieldRenderer} label="Tenure" validate={[required, number]}/>
                    <Field name="gender" component={ReduxFormDropdownRenderer} label="Gender" options={this.genderOptions} validate={[required]}/>
                </div>
                <div className={classes.formFooter}>
                    <NavLink to="/">
                        <DefaultButton
                            className={`${classes.formButton} ${classes.formButtonBack}`}
                            iconProps={this.backButtonIconProps}
                            text="Back to dashboard"
                        />
                    </NavLink>

                    <DefaultButton
                        className={`${classes.formButton} ${classes.formButtonStorno}`}
                        iconProps={this.stornoButtonIconProps}
                        text="Reset"
                        disabled={pristine || submitting}
                        onClick={reset}
                    />

                    <DefaultButton
                        className={`${classes.formButton} ${classes.formButtonSave}`}
                        iconProps={this.saveButtonIconProps}
                        text="Save"
                        type="submit"
                        disabled={pristine || invalid || submitting || submitSucceeded}
                        primary
                    />
                </div>

                { submitting && !submitSucceeded ? (
                    <h2 className={classes.formResult}>
                        <Spinner size={SpinnerSize.medium} />
                        <span className={classes.formResultText}>Creating new Employee</span>
                    </h2>
                ) : null }

                { submitSucceeded ? (
                    <h2 className={classes.formResult}>
                        <Icon iconName="Accept" className={classes.formResultIcon} />
                        <span className={classes.formResultText}>Employee successfully created</span>
                    </h2>
                ) : null }
            </form>
        );
    }
}
