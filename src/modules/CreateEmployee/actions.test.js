import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { SUBMIT_FORM_REQUEST }   from './actionTypes';
import { sortedNaturaly, newDataItem } from './mock';
import { onSubmitFormRequest, onSubmitFormSuccess } from './actions';
import { NAME } from './constants';
import { initialState } from './reducer';
import employeesModule from '../Employees';
const { ADD_DATA_ITEM } = employeesModule.actionTypes;


const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);


describe('CreateEmployee actions', () => {
    it(`onSubmitFormRequest should fire proper ${SUBMIT_FORM_REQUEST} action`, () => {
        const expectedActions = [
            { type: SUBMIT_FORM_REQUEST, payload: { data: newDataItem } },
            { type: ADD_DATA_ITEM, payload: { data: newDataItem } },
        ];

        const store = mockStore({ [NAME]: initialState });

        return store.dispatch(onSubmitFormRequest(newDataItem)).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it(`onSubmitFormSuccess should fire proper ${ADD_DATA_ITEM} action`, () => {
        const expectedAction = { type: ADD_DATA_ITEM, payload: { data: newDataItem } };
        const actualAction = onSubmitFormSuccess(newDataItem);

        expect(actualAction).toEqual(expectedAction);
    });
});


