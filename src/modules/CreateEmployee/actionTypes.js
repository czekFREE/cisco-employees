// @flow
import { NAME } from './constants';

export const SUBMIT_FORM_REQUEST = `${NAME}/SUBMIT_FORM_REQUEST`;
export const SUBMIT_FORM_SUCCESS = `${NAME}/SUBMIT_FORM_SUCCESS`;
