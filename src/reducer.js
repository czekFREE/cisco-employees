// @flow
import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import appBar from './components/AppBar';
import employeesModule from './modules/Employees';

const appBarName = appBar.constants.NAME;
const appBarReducer = appBar.reducer;

const employeesName = employeesModule.constants.NAME;
const employeesReducer = employeesModule.reducer;


/**
 * ROOT REDUCER
 */
const rootReducer = combineReducers({
    [appBarName]: appBarReducer,
    [employeesName]: employeesReducer,
    router,
    form: formReducer,
});

export default rootReducer;
