import React, { Component } from 'react';
import { Switch, Route } from 'react-router';
import employeesModule from './modules/Employees';
import createEmployeeModule from './modules/CreateEmployee';


const { Employees } = employeesModule.components;
const { CreateEmployee } = createEmployeeModule.components;

export default class Routes extends Component {
    render() {
        return (
            <Switch>
                <Route path="/employees/create" component={CreateEmployee} />
                <Route path="/" component={Employees} />
                {/* <Route path="*" component={NotFound} /> */}
            </Switch>
        );
    }
}
