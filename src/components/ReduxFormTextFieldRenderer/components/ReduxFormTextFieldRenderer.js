// @flow
import React, { Component } from 'react';
import { TextField } from 'office-ui-fabric-react';


type Props = {};


export default class ReduxFormTextFieldRenderer extends Component<Props> {
    onChanged = (value) => {
        return this.props.input.onChange(value);
    }

    render() {
        const { input, meta = {}, ...custom } = this.props;
        const { error, visited } = meta;

        return (
            <TextField
                onChanged={this.onChanged}
                errorMessage={visited && error}
                {...input}
                {...custom}
            />
        );
    }
}
