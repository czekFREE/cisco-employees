import * as actions from './actions';
import * as constants from './constants';
import reducer from './reducer';
import components from './components';
import * as selectors from './selectors';

export default { actions, reducer, components, constants, selectors };
