// @flow
import React, { Component } from 'react';
import logo from '../../../images/cisco-logo-blue.png';

import classes from './AppBar.css';
import { NavLink } from 'react-router-dom';

type Props = {
    pathname: string,
};

export default class AppBar extends Component<Props> {
    props: Props;

    getHeadingText = () => {
        const { pathname } = this.props;

        let headingText;

        if (pathname === '/employee/create') {
            headingText = 'Create employee';
        } else {
            headingText = 'Employees dashboard';
        }

        return headingText;
    }

    render() {
        return (
            <div className={`AppBar ${classes.AppBar}`}>
                <NavLink
                    to="/"
                    className={`${classes.AppBarItem} ${classes.AppBarLogo}`}
                    activeClassName={classes.logo__active}
                >
                    <img src={logo} alt="Cisco logo"/>
                </NavLink>

                <div className={`${classes.AppBarItem} ${classes.AppBarHeading}`}>
                    {this.getHeadingText()}
                </div>
            </div>
        );
    }
}
