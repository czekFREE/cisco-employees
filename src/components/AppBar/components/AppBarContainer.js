import { bindActionCreators }   from 'redux';
import { connect }              from 'react-redux';
import { withRouter }           from 'react-router';
import AppBar                   from './AppBar';
import * as actions             from '../actions';
import { getPathName }          from '../selectors';


const mapStateToProps = (state) => ({
    pathname: getPathName(state),
});


const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AppBar));
