import React from 'react';
import AppBar from './AppBar';
import { shallow } from 'enzyme';


describe('<AppBar />', () => {
    it('renders', () => {
        const wrapper = shallow(<AppBar />);
        expect(wrapper.find('.AppBar').length).toBe(1);
    });
});
