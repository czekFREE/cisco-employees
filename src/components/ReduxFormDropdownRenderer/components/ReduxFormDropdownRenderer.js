// @flow
import { Dropdown } from 'office-ui-fabric-react';
import React, { PureComponent } from 'react';


type Props = {};


export default class ReduxFormDropdownRenderer extends PureComponent<Props> {
    onChanged = value => {
        const { input } = this.props;
        input.onChange(value.key);
    }

    render() {
        const { input = {}, meta = {}, children, ...custom } = this.props;
        const { error, visited } = meta;

        return (
            <Dropdown
                errorText={visited && error}
                onChanged={this.onChanged}
                children={children}
                selectedKey={input.value}
                {...input}
                {...custom}
            />
        );
    }
}
