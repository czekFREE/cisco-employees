// @flow
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ReduxFormDropdownRenderer from './ReduxFormDropdownRenderer';
import * as actions from '../actions';


function mapStateToProps(state) {
    return {};
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ReduxFormDropdownRenderer);
