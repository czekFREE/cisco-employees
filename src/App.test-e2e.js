import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

const puppeteer = require('puppeteer');

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
});

describe('App build', () => {
    test('is served on localhost port 3000', async () => {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();
        let errors = [];

        page.on('pageError', err => errors.push(err));
        page.on('error', err => errors.push(err));

        try {
            await page.goto('http://localhost:3000');
        } catch (err) {
            throw err;
        }

        const rootUrl = await page.url();

        expect(rootUrl).toEqual('http://localhost:3000');
        expect(errors.length).toEqual(0);

        const navigationPromise = page.waitForNavigation();
        await page.click('#create-employee-button');
        await navigationPromise;

        const createEmployeeUrl = await page.url();

        expect(createEmployeeUrl).toEqual('http://localhost:3000/employees/create');

        await browser.close();
    }, 60000);
});
